const puppeteer = require('puppeteer-extra');
const StealthPlugin = require('puppeteer-extra-plugin-stealth');
puppeteer.use(StealthPlugin());
const fs = require('fs');
const process = require('process');

if (process.argv.length < 4) {
  console.log("Usage: node search.js <specificString> \"query1,query2,query3,...\"");
  process.exit(1);
}

const specificString = process.argv[2];
const queries = process.argv[3].split(',').map(q => encodeURIComponent(q.trim()));

const maxPages = 100;
const debugPath = "/app/debug";
const baseUrl = "https://www.upwork.com/nx/search/talent";
const filters = "top_rated_plus=yes";

const searchForString = async () => {
  const browser = await puppeteer.launch({ headless: true });
  let summary = [];

  for (let query of queries) {
    let found = false;
    for (let pageNum = 1; pageNum <= maxPages; pageNum++) {
      const queryDecoded = decodeURIComponent(query);
      console.log(`Navigating to page ${pageNum} for query "${queryDecoded}"...`);

      const page = await browser.newPage();
      await page.goto(`${baseUrl}/?nbs=1&q=${query}&${filters}&page=${pageNum}`);

      const html = await page.content();

      if (html.includes(specificString)) {
        console.log(`Found "${specificString}" on page ${pageNum} for query "${queryDecoded}"`);
        await page.screenshot(
          {
            path: `${debugPath}/${specificString}-${query}-${pageNum}.png`,
            fullPage: true
          }
        );
        summary.push({ query: query, page: pageNum });
        found = true;
        await page.close();
        break;
      }

      await page.close();
    }

    if (!found) summary.push({ query: query, page: "Not Found" });
  }

  await browser.close();

  console.log(`Search Summary:\nPage\tQuery\tPattern`);
  summary.forEach(item => {
    console.log(`${item.page}\t${decodeURIComponent(item.query)}\t${specificString}`);
  });
};

searchForString().then(() => console.log("Search Completed"));
