FROM ghcr.io/puppeteer/puppeteer:22

RUN npm install puppeteer-extra puppeteer-extra-plugin-stealth
COPY search.js search.js
