# Upwork Talent Page search

Searches for specific pattern via Upwork Talent Search and outputs page number
where the pattern was found.

# Usage

```
docker-compose run --rm app search "John S." "javascript developer,devops"
```

Screenshots are saved in the `debug` directory.

# Cleanup
```
docker-compose down
```
